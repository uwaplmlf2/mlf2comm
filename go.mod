module bitbucket.org/uwaplmlf2/mlf2comm

go 1.17

require (
	bitbucket.org/uwaplmlf2/go-mlf2 v1.5.2
	bitbucket.org/uwaplmlf2/go-shell v0.7.0-beta.1
	github.com/google/shlex v0.0.0-20191202100458-e7afc7fbc510
	golang.org/x/sys v0.0.0-20220408201424-a24fb2fb8a0f
)

require github.com/pkg/errors v0.8.1 // indirect
