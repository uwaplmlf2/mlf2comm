package main

import (
	"testing"
	"testing/fstest"
	"time"
)

func TestFileSortOldest(t *testing.T) {
	// Use the Sys attribute to store the expected order
	testFsys := fstest.MapFS{
		"f0001": {ModTime: time.Unix(1000, 0), Sys: int(2)},
		"f0002": {ModTime: time.Unix(1, 0), Sys: int(1)},
		"f0003": {ModTime: time.Unix(1001, 0), Sys: int(3)},
		"f0004": {ModTime: time.Unix(0, 0), Sys: int(0)},
	}
	list, err := listFiles(testFsys, oldestFirst)
	if err != nil {
		t.Fatal(err)
	}

	for i, fi := range list {
		j := fi.Sys().(int)
		if j != i {
			t.Errorf("Wrong index; expected %d, got %d", i, j)
		}
	}
}

func TestFileSortNewest(t *testing.T) {
	// Use the Sys attribute to store the expected order
	testFsys := fstest.MapFS{
		"f0001": {ModTime: time.Unix(1000, 0), Sys: int(1)},
		"f0002": {ModTime: time.Unix(1, 0), Sys: int(2)},
		"f0003": {ModTime: time.Unix(1001, 0), Sys: int(0)},
		"f0004": {ModTime: time.Unix(0, 0), Sys: int(3)},
	}
	list, err := listFiles(testFsys, newestFirst)
	if err != nil {
		t.Fatal(err)
	}

	for i, fi := range list {
		j := fi.Sys().(int)
		if j != i {
			t.Errorf("Wrong index; expected %d, got %d", i, j)
		}
	}
}

func TestFileSortWithHidden(t *testing.T) {
	// Use the Sys attribute to store the expected order
	testFsys := fstest.MapFS{
		"f0001":  {ModTime: time.Unix(1000, 0), Sys: int(1)},
		".f0002": {ModTime: time.Unix(1, 0), Sys: int(-1)},
		"f0003":  {ModTime: time.Unix(1001, 0), Sys: int(0)},
		"f0004":  {ModTime: time.Unix(0, 0), Sys: int(2)},
	}
	list, err := listFiles(testFsys, newestFirst)
	if err != nil {
		t.Fatal(err)
	}

	if len(list) != len(testFsys)-1 {
		t.Errorf("Unexpected list length: %d", len(list))
	}

	for i, fi := range list {
		j := fi.Sys().(int)
		if j != i {
			t.Errorf("Wrong index; expected %d, got %d", i, j)
		}
	}
}
