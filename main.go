// Mlf2comm manages the communication session with the Shore Server via a
// TCP connection.
package main

import (
	"bytes"
	"context"
	"encoding/xml"
	"flag"
	"fmt"
	"io"
	"log"
	"net"
	"os"
	"os/exec"
	"os/signal"
	"path/filepath"
	"runtime"
	"strconv"
	"strings"
	"syscall"
	"time"

	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/params"
	"bitbucket.org/uwaplmlf2/mlf2comm/internal/cmdshell"
)

const Usage = `Usage: mlf2comm [options] imei host:port

Manage a communications session with the Shore Server at host:port, imei is the
Iridium modem ID.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	outBox    string        = "/data/OUTBOX"
	qDir      string        = "/data/OUTGOING"
	inBox     string        = "/data/INBOX"
	dataDir   string        = "/data/ARCHIVE"
	fwdFile   string        = "/data/FORWARD/cmds.txt"
	paramFile string        = "/data/INBOX/params.xml"
	modemGpio int           = 117
	sendCmd   string        = "sz -8"
	rdTimeo   time.Duration = time.Second * 10
	commTimeo time.Duration = time.Second * 1000
	debugXfer bool
	tcpWin    int = 11 * 1024
)

func readUntil(rdr io.Reader, match []byte) (string, error) {
	c := make([]byte, 1)
	buf := make([]byte, 0)
	n, err := rdr.Read(c)
	for n > 0 {
		buf = append(buf, c[0])
		if bytes.HasSuffix(buf, match) {
			break
		}
		n, err = rdr.Read(c)
		if err != nil {
			break
		}
	}
	return string(buf), err
}

// Log-in to the RUDICS gateway and verify the connection to the server by
// waiting for a prompt.
func rudicsLogin(conn net.Conn, token string, tlimit time.Duration) error {
	conn.SetReadDeadline(time.Now().Add(tlimit))
	defer conn.SetReadDeadline(time.Time{})
	_, err := readUntil(conn, []byte("irsn: "))
	if err != nil {
		return err
	}
	fmt.Fprintf(conn, "%s\n", token)
	return getPrompt(conn, tlimit)
}

// Read from conn until the server prompt is seen or the timeout expires
func getPrompt(conn net.Conn, tlimit time.Duration) error {
	conn.SetReadDeadline(time.Now().Add(tlimit))
	defer conn.SetReadDeadline(time.Time{})
	_, err := readUntil(conn, []byte("?"))
	return err
}

// Send the contents of an MLF2 status file to the server
func sendStatus(infile string, wtr io.Writer) error {
	status, err := os.ReadFile(infile)
	if err != nil {
		return err
	}
	// The status message contents must be terminated with a linefeed, trim
	// any existing one from the file contents first.
	_, err = fmt.Fprintf(wtr, "status\napplication/xml\n%s\n",
		bytes.TrimRight(status, " \r\n"))
	return err
}

// Run an external process with its standard input and output redirected to
// rw. Returns the process exit code along with any error that occurs.
func redirectCmd(ctx context.Context, rw io.ReadWriter, cmdline []string) (int, error) {
	cmd := exec.CommandContext(ctx, cmdline[0], cmdline[1:]...)
	cmd.Stdin = rw
	cmd.Stdout = rw
	if debugXfer {
		cmd.Stderr = os.Stderr
	}

	var (
		code int
		err  error
	)

	if err = cmd.Run(); err != nil {
		if exitError, ok := err.(*exec.ExitError); ok {
			code = exitError.ExitCode()
		}
	}

	return code, err
}

// Send a single file to the server using ZMODEM.
func sendFile(ctx context.Context, infile string, conn net.Conn) error {
	base := filepath.Base(infile)
	cmdline := strings.Split(sendCmd, " ")
	cmdline = append(cmdline, infile)

	fmt.Fprintf(conn, "zput %s\n", base)
	_, err := redirectCmd(ctx, conn, cmdline)
	return err
}

// Send a list of files to the server. Each file is removed after a
// successful transfer.
func sendFiles(ctx context.Context, files []string, conn net.Conn, tlimit time.Duration) (int, error) {
	var count int

	for _, path := range files {
		select {
		case <-ctx.Done():
			return count, ctx.Err()
		default:
		}

		fi, err := os.Stat(path)
		if err != nil {
			log.Printf("Skipping file %q: %v", path, err)
			continue
		}

		log.Printf("Sending %s", path)
		t0 := time.Now()
		err = sendFile(ctx, path, conn)
		if err != nil {
			return count, fmt.Errorf("Upload failed %q: %w", path, err)
		}
		count++
		dt := time.Now().Sub(t0)
		log.Printf("%d bytes in %.3f seconds", fi.Size(), dt.Seconds())
		os.Remove(path)
		conn.Write([]byte("\n"))
		if err := getPrompt(conn, tlimit); err != nil {
			return count, fmt.Errorf("No prompt: %w", err)
		}
	}

	return count, nil
}

// Upload all files in the directory dir to the server. Tlimit specifies the
// maximum time to wait for a server prompt between file transfers. The
// function returns when all files are transferred or the Context
// expires. Returns the number of files sent and any error that occured.
func sendOutbox(ctx context.Context, dir string, conn net.Conn, tlimit time.Duration) (int, error) {
	files, err := listDir(dir, oldestFirst)
	if err != nil {
		return 0, err
	}

	// Continue rereading the directory and sending files to catch
	// any files added to the directory during the transfer.
	total := int(0)
	for len(files) > 0 {
		log.Printf("Found %d files", len(files))
		n, err := sendFiles(ctx, files, conn, tlimit)
		total += n
		if err != nil {
			return total, err
		}
		files, err = listDir(dir, oldestFirst)
		if err != nil {
			return total, err
		}
	}

	return total, nil
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&outBox, "out", lookupEnvOrString("OUTBOX", outBox),
		"Set OUTBOX directory")
	flag.StringVar(&qDir, "queue", lookupEnvOrString("OUTGOING", qDir),
		"Set output file queue directory")
	flag.StringVar(&dataDir, "data", lookupEnvOrString("ARCHIVE", dataDir),
		"Set data archive directory")
	flag.StringVar(&inBox, "in", lookupEnvOrString("INBOX", inBox),
		"Set INBOX directory")
	flag.StringVar(&fwdFile, "fwd", lookupEnvOrString("MSGFILE", fwdFile),
		"Set message forwarding file")
	flag.StringVar(&paramFile, "params", paramFile, "Set mission parameter file")
	flag.StringVar(&sendCmd, "send", sendCmd, "Set ZMODEM file send command")
	flag.IntVar(&modemGpio, "gpio", modemGpio, "Set modem status GPIO line")
	flag.DurationVar(&rdTimeo, "rd-timeout", rdTimeo, "Set read timeout")
	flag.DurationVar(&commTimeo, "tlimit", commTimeo, "Set session time limit")
	flag.BoolVar(&debugXfer, "v", debugXfer,
		"More verbose output for file transfers")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

// Load an MLF2 parameter table file.
func loadPtable(filename string) (*params.Ptable, error) {
	f, err := os.Open(filename)
	if err != nil {
		return nil, fmt.Errorf("open %q: %w", filename, err)
	}
	defer f.Close()

	var pt params.PtableList
	if err = xml.NewDecoder(f).Decode(&pt); err != nil {
		return nil, fmt.Errorf("decode %q: %w", filename, err)
	}

	return &pt.Ptable, nil
}

// Start a cmdshell.Cmdshell to read messages (commands) from the server
func getMsgs(ctx context.Context, conn net.Conn, pt *params.Ptable,
	tlimit time.Duration) (int, error) {

	ffwd, err := os.Create(fwdFile)
	if err != nil {
		log.Printf("WARNING: Cannot open forwarding file: %v", err)
	} else {
		defer ffwd.Close()
	}

	shell := cmdshell.New(conn, pt,
		cmdshell.QueueDir(outBox),
		cmdshell.DataDir(dataDir),
		cmdshell.FwdInterface(ffwd),
	)

	if tlimit > 0 {
		conn.SetReadDeadline(time.Now().Add(tlimit))
		defer conn.SetReadDeadline(time.Time{})
	}
	return shell.Run(ctx)
}

func main() {
	args := parseCmdLine()
	if len(args) < 2 {
		fmt.Fprint(os.Stderr, "Missing command-line arguments\n")
		flag.Usage()
		os.Exit(1)
	}

	// Return a proper exit code to the OS
	var exitCode int
	defer func() { os.Exit(exitCode) }()

	// Drop log timestamps when running under Systemd
	if _, ok := os.LookupEnv("JOURNAL_STREAM"); ok {
		log.SetFlags(0)
	}

	conn, err := net.Dial("tcp", args[1])
	if err != nil {
		log.Printf("Cannot connect to %s: %v", args[1], err)
		exitCode = 1
		return
	}
	defer conn.Close()

	// Set the TCP write buffer
	conn.(*net.TCPConn).SetWriteBuffer(tcpWin)

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals
	go func() {
		s := <-sigs
		signal.Stop(sigs)
		log.Printf("Got signal: %v", s)
		cancel()
		conn.Close()
	}()

	if err := rudicsLogin(conn, args[0], rdTimeo); err != nil {
		log.Printf("Login failed: %v", err)
		exitCode = 1
		return
	}

	// Send status info
	statFile := filepath.Join(outBox, "status.xml")
	if err := sendStatus(statFile, conn); err != nil {
		log.Printf("Cannot send status: %v", err)
	} else {
		os.Remove(statFile)
		if err := getPrompt(conn, rdTimeo); err != nil {
			log.Printf("No prompt: %v", err)
			exitCode = 1
			return
		}
	}

	pt, err := loadPtable(paramFile)
	if err != nil {
		log.Printf("WARNING: Cannot load parameter table: %v", err)
	}

	// Check if a comm session timeout is specified in the parameter
	// file provided by the TT8
	if pt != nil {
		if p := pt.Lookup("comm:timeout"); p.Name != "" {
			if val, err := strconv.Atoi(p.Value); err == nil {
				commTimeo = time.Duration(val) * time.Second
				log.Printf("New session timeout: %v", commTimeo)
			}
		}
	}

	// Set a time limit for the remainder of the session
	if commTimeo > 0 {
		ctx, cancel = context.WithTimeout(ctx, commTimeo)
		defer cancel()
	}

	// Get messages (commands) from shore
	exitCode, err = getMsgs(ctx, conn, pt, time.Second*30)
	if err != nil {
		log.Printf("getMsgs: %v", err)
	}

	// Verify server response
	if err := getPrompt(conn, rdTimeo); err != nil {
		log.Printf("No prompt: %v", err)
		exitCode = 1
		return
	}

	// Send all queued files
	n, err := sendOutbox(ctx, outBox, conn, rdTimeo)
	if err != nil {
		log.Println(err)
		exitCode = 1
		return
	}

	log.Printf("Transferred %d files", n)
	conn.Write([]byte("\n"))
	if err := getPrompt(conn, rdTimeo); err != nil {
		log.Printf("No prompt: %v", err)
		exitCode = 1
		return
	}
	conn.Write([]byte("bye\n"))
}
