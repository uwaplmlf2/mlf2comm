// Package gpio allows control of GPIO pins via the Linux sysfs interface

// +build linux

package gpio

import (
	"errors"
	"fmt"
	"os"
	"strconv"
	"syscall"
	"time"

	"golang.org/x/sys/unix"
)

type GpioDir string

const (
	In  GpioDir = "in"
	Out         = "out"
)

type GpioVal int

const (
	Low  GpioVal = 0
	High         = 1
)

type GpioEdge string

const (
	Rising  GpioEdge = "rising"
	Falling          = "falling"
)

const (
	// GPIOPATH default linux gpio path
	GPIOPATH = "/sys/class/gpio"
)

var ErrNotExported = errors.New("pin has not been exported")
var ErrTimeout = errors.New("Operation timed out")

type Pin struct {
	pin       string
	label     string
	value     *os.File
	direction *os.File
	valPath   string
}

func NewPin(pin int) *Pin {
	p := &Pin{
		pin: strconv.Itoa(pin),
	}
	p.label = "gpio" + p.pin

	return p
}

func (p *Pin) String() string {
	return p.label
}

func (p *Pin) Out() error {
	if err := p.Export(); err != nil {
		return err
	}
	return p.Direction(Out)
}

func (p *Pin) In() error {
	if err := p.Export(); err != nil {
		return err
	}
	return p.Direction(In)
}

// Direction configures the pin to be input or output
func (p *Pin) Direction(dir GpioDir) error {
	_, err := writeFile(p.direction, []byte(dir))
	return err
}

// Write sets the value of an output pin
func (p *Pin) Write(val GpioVal) error {
	_, err := writeFile(p.value, []byte(strconv.Itoa(int(val))))
	return err
}

// Read returns the value of an input pin
func (p *Pin) Read() (GpioVal, error) {
	buf, err := readFile(p.value)
	if err != nil {
		return 0, err
	}
	x, err := strconv.Atoi(string(buf[0]))
	return GpioVal(x), err
}

// Export allows this pin to be used for I/O
func (p *Pin) Export() error {
	export, err := os.OpenFile(GPIOPATH+"/export", os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer export.Close()

	_, err = writeFile(export, []byte(p.pin))
	if err != nil {
		// If EBUSY then the pin has already been exported
		e, ok := err.(*os.PathError)
		if !ok || e.Err != syscall.EBUSY {
			return err
		}
	}

	if p.direction != nil {
		p.direction.Close()
	}

	attempt := 0
	for {
		attempt++
		p.direction, err = os.OpenFile(fmt.Sprintf("%v/%v/direction", GPIOPATH, p.label), os.O_RDWR, 0644)
		if err == nil {
			break
		}
		if attempt > 10 {
			return err
		}
		time.Sleep(10 * time.Millisecond)
	}

	if p.value != nil {
		p.value.Close()
	}
	if err == nil {
		p.valPath = fmt.Sprintf("%v/%v/value", GPIOPATH, p.label)
		p.value, err = os.OpenFile(p.valPath, os.O_RDWR, 0644)
	}

	if err != nil {
		p.Unexport()
	}

	return err
}

// Unexport releases the pin
func (p *Pin) Unexport() error {
	unexport, err := os.OpenFile(GPIOPATH+"/unexport", os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	defer unexport.Close()

	if p.direction != nil {
		p.direction.Close()
		p.direction = nil
	}
	if p.value != nil {
		p.value.Close()
		p.value = nil
	}

	_, err = writeFile(unexport, []byte(p.pin))
	if err != nil {
		// If EINVAL then the pin is reserved in the system and can't be unexported
		e, ok := err.(*os.PathError)
		if !ok || e.Err != syscall.EINVAL {
			return err
		}
	}

	return nil
}

// WaitForEdge waits for a pin state transition or for the timeout to expire. Set
// timeout to -1 to wait forever.
func (p *Pin) WaitForEdge(edge GpioEdge, timeout time.Duration) error {
	if p.direction == nil {
		return ErrNotExported
	}

	if err := p.Direction(In); err != nil {
		return err
	}

	f, err := os.OpenFile(fmt.Sprintf("%v/%v/edge", GPIOPATH, p.label), os.O_RDWR, 0644)
	if err != nil {
		return err
	}
	defer f.Close()

	_, err = f.Write([]byte(edge))
	fds := []unix.PollFd{
		{
			Fd:     int32(p.value.Fd()),
			Events: unix.POLLPRI,
		},
	}

	t := int(-1)
	if timeout >= 0 {
		t = int(timeout.Milliseconds())
	}

	readFile(p.value)
	n, err := unix.Poll(fds, t)
	if err == nil && n == 0 {
		err = ErrTimeout
	}

	return err
}

func writeFile(f *os.File, data []byte) (int, error) {
	if f == nil {
		return 0, ErrNotExported
	}

	n, _ := f.Write(data)
	return n, f.Sync()
}

func syncWriteFile(path string, data []byte) (int, error) {
	f, err := os.OpenFile(path, os.O_RDWR, 0644)
	if err != nil {
		return 0, err
	}
	defer f.Close()
	return f.Write(data)
}

func readFile(f *os.File) ([]byte, error) {
	if f == nil {
		return nil, ErrNotExported
	}

	buf := make([]byte, 2)
	_, err := f.Seek(0, os.SEEK_SET)
	if err == nil {
		_, err = f.Read(buf)
	}
	return buf, err
}
