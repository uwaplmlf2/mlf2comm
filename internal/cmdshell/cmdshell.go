// Cmdshell provides a struct which responds to commands sent from the
// MLF2 Shore Server. The Shore Server is sent the string "msgs?\n" and
// it responds with one linefeed terminated command and expects a linefeed
// terminated response. A blank command terminates the transaction.
//
//    Shell                  Server
//    -----                  ------
//
//    "msgs?\n"   --->
//                <---       message-1
//    response-1  --->
//                <---       message-2
//    response-2  --->
//                <---       "\n"
//    EXIT
package cmdshell

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/params"
	shell "bitbucket.org/uwaplmlf2/go-shell"
	"github.com/google/shlex"
)

const PROMPT = "msgs?\n"

var ErrArg = errors.New("Missing command argument")

// Exit status codes
const (
	WaitMode int = 2
	Continue int = 4
)

type Cmdshell struct {
	*shell.Shell
	pt        *params.Ptable
	fwd       io.Writer
	exitCode  int
	outDir    string
	qDir      string
	dataDir   string
	dirLister string
	fstart    func()
}

type Option struct {
	f func(*shelloptions)
}

type shelloptions struct {
	outDir, dataDir, qDir string
	dirLister             string
	fwd                   io.Writer
}

// Set the directory for outgoing files. Files written to this directory
// are automatically compressed and added to the outgoing file queue.
func OutDir(path string) Option {
	return Option{func(opts *shelloptions) {
		opts.outDir = path
	}}
}

// Set the directory for outgoing file queue
func QueueDir(path string) Option {
	return Option{func(opts *shelloptions) {
		opts.qDir = path
	}}
}

// Set the data archive directory
func DataDir(path string) Option {
	return Option{func(opts *shelloptions) {
		opts.dataDir = path
	}}
}

// Set the program used to produce a directory listing.
func DirLister(cmd string) Option {
	return Option{func(opts *shelloptions) {
		opts.dirLister = cmd
	}}
}

// Set the io.Writer that will receive the commands to be forwarded
// to the TT8.
func FwdInterface(w io.Writer) Option {
	return Option{func(opts *shelloptions) {
		opts.fwd = w
	}}
}

func New(rw io.ReadWriter, pt *params.Ptable, options ...Option) *Cmdshell {
	opts := shelloptions{
		outDir:    "/data/OUTGOING",
		qDir:      "/data/OUTBOX",
		dataDir:   "/data/ARCHIVE",
		dirLister: "tree -D -t",
	}
	for _, opt := range options {
		opt.f(&opts)
	}

	cs := Cmdshell{
		Shell:     shell.New(rw, "", shell.ExitOnEmpty(true), shell.Strict(true)),
		pt:        pt,
		fwd:       opts.fwd,
		outDir:    opts.outDir,
		qDir:      opts.qDir,
		dataDir:   opts.dataDir,
		dirLister: opts.dirLister,
	}

	if pt != nil {
		cs.Shell.AddCommand("get", cs.GetParam)
		cs.Shell.AddCommand("set", cs.SetParam)
	}
	cs.Shell.AddCommand("send-file", cs.SendFile)
	cs.Shell.AddCommand("del", cs.DelFile)
	cs.Shell.AddCommand("dir", cs.DirListing)
	cs.Shell.AddCommand("wait", cs.FwdCommand("wait", WaitMode))

	// Forward command to TT8 and leave the exit status unchanged
	for _, name := range []string{"flasher-on", "flasher-off", "safe", "dump", "release", "adcp:"} {
		cs.Shell.AddCommand(name, cs.FwdCommand(name, -1))
	}

	// Forward command to TT8 and change the exit status to Continue
	for _, name := range []string{"continue", "recover", "restart", "abort"} {
		cs.Shell.AddCommand(name, cs.FwdCommand(name, Continue))
	}

	// Function to prompt the server to start sending commands
	cs.fstart = func() { rw.Write([]byte(PROMPT)) }

	return &cs
}

func (cs *Cmdshell) Run(ctx context.Context) (int, error) {
	cs.fstart()
	err := cs.Shell.Run(ctx)
	return cs.exitCode, err
}

// GetParam outputs the current value of an MLF2 mission parameter
func (cs *Cmdshell) GetParam(ctx context.Context, args ...string) (string, error) {
	if len(args) < 1 {
		return "", fmt.Errorf("get: %w", ErrArg)
	}

	p := cs.pt.Lookup(args[0])
	if p.Name == "" {
		return "", fmt.Errorf("Unknown parameter: %q", args[0])
	}
	return fmt.Sprintf("%s=%s", args[0], p.Value), nil
}

// SetParam updates the value of an MLF2 mission parameter
func (cs *Cmdshell) SetParam(ctx context.Context, args ...string) (string, error) {
	if len(args) < 2 {
		return "", fmt.Errorf("set: %w", ErrArg)
	}

	p := cs.pt.Lookup(args[0])
	if p.Name == "" {
		return "", fmt.Errorf("Unknown parameter: %q", args[0])
	}
	if cs.fwd != nil {
		fmt.Fprintf(cs.fwd, "set %s\n", strings.Join(args, " "))
	}
	p.Value = args[1]
	cs.pt.Update(args[0], p)
	return fmt.Sprintf("%s=%s", args[0], p.Value), nil
}

// SendFile links an archived file into the outbox so it can be sent to the shore
// server
func (cs *Cmdshell) SendFile(ctx context.Context, args ...string) (string, error) {
	if len(args) < 1 {
		return "", fmt.Errorf("send-file: %w", ErrArg)
	}

	pathname := filepath.Join(cs.dataDir, args[0])
	err := os.Link(pathname, filepath.Join(cs.outDir, filepath.Base(args[0])))
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s added to queue", args[0]), nil
}

// DelFile removes a file from the outbox
func (cs *Cmdshell) DelFile(ctx context.Context, args ...string) (string, error) {
	if len(args) < 1 {
		return "", fmt.Errorf("del: %w", ErrArg)
	}

	pathname := filepath.Join(cs.qDir, args[0])
	err := os.Remove(pathname)
	if err != nil {
		return "", err
	}
	return fmt.Sprintf("%s deleted", pathname), nil
}

func runProg(cmdline string) ([]byte, error) {
	args, err := shlex.Split(cmdline)
	if err != nil {
		return []byte(""), err
	}
	return exec.Command(args[0], args[1:]...).Output()
}

// DirListing creates a file named 00index.txt containing a directory listing
// of the data archive and copies that file to the outbox.
func (cs *Cmdshell) DirListing(ctx context.Context, args ...string) (string, error) {
	var dir string
	if len(args) < 1 {
		dir = cs.dataDir
	} else {
		dir = filepath.Join(cs.dataDir, args[0])
	}

	output, err := runProg(cs.dirLister + " " + dir)
	if err != nil {
		return "", fmt.Errorf("dir listing: %w", err)
	}

	// Save the listing to a file named 00index in the outgoing directory
	if f, err := os.Create(filepath.Join(cs.qDir, "00index.txt")); err == nil {
		f.Write(output)
		f.Close()
	} else {
		return "", fmt.Errorf("write listing file: %w", err)
	}
	return fmt.Sprintf("Listing of %q queued", dir), nil
}

// FwdCommand forwards a command to the TT8
func (cs *Cmdshell) FwdCommand(name string, ec int) shell.Command {
	return shell.Command(func(ctx context.Context, args ...string) (string, error) {
		if cs.fwd != nil {
			cs.fwd.Write([]byte(name))
			if len(args) > 0 {
				fmt.Fprintf(cs.fwd, " %s", strings.Join(args, " "))
			}
			cs.fwd.Write([]byte("\n"))
		}

		if ec > 0 {
			cs.exitCode = ec
		}
		return "This command will be forwarded to the TT8", nil
	})
}
