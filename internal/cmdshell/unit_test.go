package cmdshell

import (
	"bufio"
	"context"
	"encoding/xml"
	"net"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	"testing"

	"bitbucket.org/uwaplmlf2/go-mlf2/pkg/params"
)

func helperCreateFiles(t *testing.T, dir string, names []string) {
	for _, name := range names {
		path := filepath.Join(dir, name)
		f, err := os.Create(path)
		if err != nil {
			t.Fatalf("Cannot create file %q: %v", path, err)
		}
		f.Close()
	}
}

func helperLoadPtable(t *testing.T, filename string) *params.Ptable {
	f, err := os.Open(filename)
	if err != nil {
		t.Fatalf("open %q: %v", filename, err)
	}

	var pt params.PtableList
	if err = xml.NewDecoder(f).Decode(&pt); err != nil {
		t.Fatal(err)
	}

	return &pt.Ptable
}

func helperStartPeer(t *testing.T, msgs []string) (net.Listener, chan string) {
	listener, err := net.Listen("tcp", ":0")
	if err != nil {
		t.Fatal(err)
	}

	ch := make(chan string, 1)
	go func() {
		conn, _ := listener.Accept()
		defer close(ch)
		rdr := bufio.NewReader(conn)
		text, err := rdr.ReadString('\n')
		if err != nil {
			t.Error(err)
			return
		}
		if text != "msgs?\n" {
			t.Errorf("Invalid request: %s", text)
			return
		}
		defer conn.Write([]byte("\n"))

		for _, msg := range msgs {
			conn.Write([]byte(msg))
			conn.Write([]byte("\n"))
			text, err = rdr.ReadString('\n')
			if err != nil {
				t.Error(err)
				return
			}
			ch <- text
		}
		conn.Write([]byte("\n"))

	}()

	return listener, ch
}

func testShell(t *testing.T, pt *params.Ptable, cmds []string,
	expected []string, code int) string {

	l, ch := helperStartPeer(t, cmds)
	defer l.Close()

	addr := l.Addr().String()
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		t.Fatal(err)
	}

	var b strings.Builder

	shell := New(conn, pt, OutDir("testdata"),
		QueueDir("testdata"), DataDir("testdata"),
		FwdInterface(&b))

	// Start goroutine to accumulate responses
	responses := make([]string, 0)
	go func() {
		for resp := range ch {
			responses = append(responses, resp)
		}
	}()

	ec, err := shell.Run(context.Background())
	if err != nil {
		t.Fatal(err)
	}

	if ec != code {
		t.Errorf("exit code; expected %d, got %d", code, ec)
	}

	if !reflect.DeepEqual(expected, responses) {
		t.Errorf("expected: %#v, got %#v", expected, responses)
	}

	return b.String()
}

func TestShell(t *testing.T) {
	cmds := []string{
		"del file1.dat",
		"set foo 42",
		"get depth_check",
		"set depth_check 10",
	}

	expected := []string{
		"testdata/file1.dat deleted\n",
		`ERROR: Unknown parameter: "foo"` + "\n",
		"depth_check=1\n",
		"depth_check=10\n",
	}

	code := int(0)
	helperCreateFiles(t, "testdata", []string{"file1.dat"})

	t.Run("shell-1", func(t *testing.T) {
		pt := helperLoadPtable(t, "testdata/params.xml")
		testShell(t, pt, cmds, expected, code)
		if p := pt.Lookup("depth_check"); p.Value != "10" {
			t.Errorf("Parameter value not changed: %#v", p)
		}
	})

	cmds = []string{"wait"}
	expected = []string{"This command will be forwarded to the TT8\n"}
	code = WaitMode
	t.Run("shell-wait", func(t *testing.T) {
		pt := helperLoadPtable(t, "testdata/params.xml")
		fwd := testShell(t, pt, cmds, expected, code)
		if fwd != "wait\n" {
			t.Errorf("Unexpected forward value: %q", fwd)
		}
	})

	cmds = []string{"flasher-off", "continue"}
	expected = []string{"This command will be forwarded to the TT8\n",
		"This command will be forwarded to the TT8\n"}
	code = Continue
	t.Run("shell-continue", func(t *testing.T) {
		pt := helperLoadPtable(t, "testdata/params.xml")
		fwd := testShell(t, pt, cmds, expected, code)
		if fwd != "flasher-off\ncontinue\n" {
			t.Errorf("Unexpected forward value: %q", fwd)
		}
	})

	cmds = []string{}
	expected = []string{}
	code = 0
	t.Run("shell-nocmds", func(t *testing.T) {
		pt := helperLoadPtable(t, "testdata/params.xml")
		testShell(t, pt, cmds, expected, code)
	})
}
