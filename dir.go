package main

import (
	"io/fs"
	"os"
	"path/filepath"
	"sort"
)

type fileSorter struct {
	info []os.FileInfo
	by   func(fi1, fi2 os.FileInfo) bool
}

func (fs *fileSorter) Len() int {
	return len(fs.info)
}

func (fs *fileSorter) Swap(i, j int) {
	fs.info[i], fs.info[j] = fs.info[j], fs.info[i]
}

func (fs *fileSorter) Less(i, j int) bool {
	return fs.by(fs.info[i], fs.info[j])
}

type By func(fi1, fi2 os.FileInfo) bool

func oldestFirst(fi1, fi2 os.FileInfo) bool {
	return fi1.ModTime().Unix() < fi2.ModTime().Unix()
}

func newestFirst(fi1, fi2 os.FileInfo) bool {
	return fi2.ModTime().Unix() < fi1.ModTime().Unix()
}

func smallestFirst(fi1, fi2 os.FileInfo) bool {
	return fi1.Size() < fi2.Size()
}

func byName(fi1, fi2 os.FileInfo) bool {
	return fi1.Name() < fi2.Name()
}

// Return a sorted list of files from the root directory of fsys.
func listFiles(fsys fs.FS, by func(f1, f2 os.FileInfo) bool) ([]os.FileInfo, error) {
	files, err := fs.ReadDir(fsys, ".")
	if err != nil {
		return nil, err
	}

	info := make([]os.FileInfo, 0, len(files))
	for _, file := range files {
		fi, err := file.Info()
		if err != nil {
			continue
		}
		if fi.IsDir() {
			continue
		}
		if fi.Name()[0] == '.' {
			continue
		}
		info = append(info, fi)
	}

	fs := &fileSorter{info: info, by: by}
	if by != nil {
		sort.Sort(fs)
	}

	return fs.info, nil
}

func listDir(dir string, by By) ([]string, error) {
	info, err := listFiles(os.DirFS(dir), by)
	if err != nil {
		return nil, err
	}

	files := make([]string, 0, len(info))
	for _, fi := range info {
		files = append(files, filepath.Join(dir, fi.Name()))
	}

	return files, nil
}
